package com.htc.ontology.semanticwebrecommendation.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/semantics")
public class Ontology {

    @GetMapping
    String getView(Model model) {
        return "semanticSearch";
    }
}
