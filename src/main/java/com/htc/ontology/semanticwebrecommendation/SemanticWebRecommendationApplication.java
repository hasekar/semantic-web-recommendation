package com.htc.ontology.semanticwebrecommendation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SemanticWebRecommendationApplication {

	public static void main(String[] args) {
		SpringApplication.run(SemanticWebRecommendationApplication.class, args);
	}
}
